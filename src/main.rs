#[allow(dead_code)]
mod event;
mod twitch;

use std::io;
use std::io::Write;
use termion::event::Key;
use termion::input::MouseTerminal;
use termion::raw::IntoRawMode;
use termion::screen::AlternateScreen;
use tui::backend::TermionBackend;
use tui::layout::{Constraint, Direction, Layout, Alignment};
use tui::widgets::{Block, Borders, Widget, SelectableList, Paragraph, Text};
use tui::Terminal;

use event::{Event, Events};

fn main() -> Result<(), failure::Error> {
    // Log file
    let mut file = std::fs::OpenOptions::new().append(true).create(true).open("tt.log")
        .expect("cannot open file");
    file.write_all(b"------\n")?;

    // Terminal initialization
    let stdout = io::stdout().into_raw_mode()?;
    let stdout = MouseTerminal::from(stdout);
    let stdout = AlternateScreen::from(stdout);
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.hide_cursor()?;

    // Setup event handlers
    let events = Events::new();

    let mut channels: Vec<twitch::Channel> = vec!();
    let mut names: Vec<String> = vec!();
    let display_current = |channels: Vec<twitch::Channel>, selected: Option<usize>| -> Vec<String> {
        match selected {
            Option::None => vec!("".to_string()),
            Option::Some(u) => vec!(channels[u].user_name.clone(), channels[u].title.clone(), channels[u].game.clone(), channels[u].viewer_count.clone(), channels[u].followers.clone())
        }
    };

    let style = tui::style::Style::default()
        .fg(tui::style::Color::Magenta)
        .bg(tui::style::Color::LightGreen);
    let mut selected: Option<usize> = Some(0);
    let mut signed_in: bool = false;
    let initial_text = "Enter your channel name here";
    let mut channel_name = initial_text.to_string();
    loop {
        terminal.draw(|mut f| {
            // Wrapping block for a group
            // Just draw the block and the group on the same area and build the group
            // with at least a margin of 1
            let size = f.size();
            Block::default().borders(Borders::ALL).render(&mut f, size);
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(1)
                .constraints([Constraint::Percentage(100), Constraint::Percentage(100)].as_ref())
                .split(f.size());
            {
                if signed_in
                {
                    let chunks = Layout::default()
                        .direction(Direction::Horizontal)
                        .margin(1)
                        .constraints([Constraint::Percentage(50),Constraint::Percentage(50)].as_ref())
                        .split(chunks[0]);
                    SelectableList::default()
                        .block(Block::default().title("Channels").borders(Borders::ALL))
                        .items(&names)
                        .select(selected)
                        .style(style)
                        .highlight_style(style.fg(tui::style::Color::Black).modifier(tui::style::Modifier::BOLD))
                        .highlight_symbol(">")
                        .render(&mut f, chunks[0]);
                    SelectableList::default()
                        .block(Block::default().title("Info").borders(Borders::ALL))
                        .items(&display_current(channels.clone(), selected))
                        .render(&mut f, chunks[1]);
                }
                else
                {
                    let chunks = Layout::default()
                        .direction(Direction::Horizontal)
                        .margin(1)
                        .constraints([Constraint::Percentage(25), Constraint::Percentage(50), Constraint::Percentage(25)].as_ref())
                        .split(chunks[0]);
                    let text = [Text::raw("Enter your channel name:\n"), Text::raw(&channel_name)];
                    Paragraph::new(text.iter())
                        .block(Block::default().title("Enter Channel").borders(Borders::ALL))
                        .style(style)
                        .alignment(Alignment::Center)
                        .render(&mut f, chunks[1]);
                }
            }
        })?;

        let up = |selected: Option<usize>| {
            if let Some(sel) = selected {
                if sel > 0 {
                    Some(sel - 1)
                } else {
                    Some(names.len() - 1)
                }
            } else {
                Some(0)
            }
        };
        let down = |selected: Option<usize>| {
            if let Some(sel) = selected {
                if sel >= names.len() - 1 {
                    Some(0)
                } else {
                    Some(sel + 1)
                }
            } else {
                Some(0)
            }
        };

        match events.next()? {
            Event::Input(key) => if signed_in
            {
                file.write_all(format!("{:?}\n", key).as_bytes())?;
                match key {
                    Key::Char('q') => {
                        break;
                    }
                    Key::Down | Key::Char('j') => {
                        selected = down(selected)
                    }
                    Key::Up | Key::Char('k') => {
                        selected = up(selected)
                    }
                    Key::Char('\n') => {
                        match selected {
                            None => (),
                            Some(u) => {
                                let mpv_command = format!("{}{}", "mpv --really-quiet http://twitch.tv/", &names[u].to_lowercase());
                                file.write_all(format!("{}\n", mpv_command).as_bytes())?;
                                match std::process::Command::new("sh")
                                    .arg("-c")
                                    .arg(mpv_command)
                                    .stdin(std::process::Stdio::null())
                                    .spawn()
                                {
                                    Result::Err(_) => (),
                                    Result::Ok(_) => ()
                                }
                            }
                        }
                    }
                    _ => {}
                }
            }
            else
            {
                match key {
                    Key::Esc => break,
                    Key::Char('\n') => {
                        signed_in = true;
                        channels = twitch::get_channel_info(&twitch::get_channel_id(&channel_name));
                        if channels.len() == 0 {
                            channels = vec!(twitch::Channel::default());
                        }
                        names = channels.iter()
                            .map(|channel| (&channel.user_name).to_string())
                            .collect::<Vec<String>>();
                    },
                    Key::Backspace => {
                        if channel_name.len() > 0
                        {
                            channel_name = channel_name[0..channel_name.len() - 1].to_string();
                        }
                    },
                    Key::Char(' ') => {}, // Space is the only forbidden character I know of, there's probably more
                    Key::Char(c) => {
                        if channel_name == initial_text {
                            channel_name = "".to_string();
                        }
                        channel_name += &c.to_string();
                    },
                    _ => {}
                }
            },
            Event::Tick => {}
        }
    }
    Ok(())
}
