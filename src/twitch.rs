use curl::easy::{Easy2, Handler, WriteError, List};
use serde_json::{Value};

struct Collector(Vec<u8>);

impl Handler for Collector {
    fn write(&mut self, data: &[u8]) -> Result<usize, WriteError> {
        self.0.extend_from_slice(data);
        Ok(data.len())
    }
}

pub struct Channel {
    pub user_name: String,
    pub title: String,
    pub game: String,
    pub viewer_count: String,
    pub followers: String,
    game_id: String,
}

impl Default for Channel {
    fn default() -> Channel {
        Channel {
            user_name: "<empty>".to_string(),
            title: "<empty>".to_string(),
            game: "<empty>".to_string(),
            viewer_count: "<empty>".to_string(),
            followers: "<empty>".to_string(),
            game_id: "<empty>".to_string(),
        }
    }
}

impl Clone for Channel {
    fn clone(&self) -> Channel {
        Channel {
            user_name: self.user_name.clone(),
            title: self.title.clone(),
            game: self.game.clone(),
            viewer_count: self.viewer_count.clone(),
            followers: self.followers.clone(),
            game_id: self.game_id.clone(),
        }
    }
}

//static MY_ID: &str = "22104027";
static CLIENT_ID: &str = "Client-ID: k2gj9yaf82i0mc58wtwak2agr73fzo";

#[allow(dead_code)]
fn twitch_api_call(url: &str) -> Result<Value, Value>
{
    let mut header_list = List::new();
    let mut error_msg: String = "Could not perform api call to ".to_owned();
    error_msg.push_str(url);
    if let std::result::Result::Err(_) = header_list.append(CLIENT_ID) {
        return Err(serde_json::json!(error_msg));
    }
    let mut handle = Easy2::new(Collector(Vec::new()));
    handle.url_encode(&url.as_bytes());

    if let std::result::Result::Err(_) = handle.url(&url) {
        return Err(serde_json::json!(error_msg));
    }
    if let std::result::Result::Err(_) = handle.http_headers(header_list) {
        return Err(serde_json::json!(error_msg));
    }
    if let std::result::Result::Err(_) = handle.perform() {
        return Err(serde_json::json!(error_msg));
    }
    let contents = handle.get_ref();
    match std::str::from_utf8(&contents.0) {
        std::result::Result::Err(_) => {
            return Err(serde_json::json!(error_msg));
        },
        Result::Ok(s) => match serde_json::from_str(s) {
            Result::Ok(j) => Ok(j),
            Result::Err(_) => {
                return Err(serde_json::json!(error_msg));
            },
        }
    }
}

fn string_or_error(val: Option<&serde_json::Value>) -> std::string::String
{
    let field_error = "Unexpected value returned";
    match val {
        Some(v) => match v {
            Value::String(s) => s.to_string(),
            Value::Number(n) => n.to_string(),
            _ => field_error.to_string()
        },
        None => field_error.to_string()
    }
}

fn get_data_array(data: &Result<serde_json::Value, serde_json::Value>) -> std::vec::Vec<Value>
{
    data.as_ref().ok()
        .and_then(|val| val.as_object())
        .and_then(|obj| obj.get("data"))
        .and_then(|v_arr| v_arr.as_array())
        .map_or(vec!(), |slice| slice.clone()) // as_array() returns Option<&Vec<..>> so need to clone
}

fn validate_follows(data: &Result<serde_json::Value, serde_json::Value>) -> std::vec::Vec<std::string::String>
{
    get_data_array(data).iter()
        .map(|obj_user|
            obj_user.get("to_id")
            .and_then(|v_str| v_str.as_str())
            .map_or("".to_string(), |s| s.to_string())
        ).collect::<Vec<std::string::String>>()
}

fn validate_streams(data: &Result<serde_json::Value, serde_json::Value>) -> std::vec::Vec<Channel>
{
    get_data_array(data).iter()
        .map(|channel| Channel {
            user_name: string_or_error(channel.get("user_name")),
            title: string_or_error(channel.get("title")),
            viewer_count: string_or_error(channel.get("viewer_count")),
            //temporarily store game id to replace with game name later
            game: string_or_error(channel.get("game_id")),
            //TODO get actual follower count
            followers: "1".to_string(),
            game_id: string_or_error(channel.get("game_id")),
        }).collect::<std::vec::Vec<Channel>>()
}

fn validate_games(data: &Result<serde_json::Value, serde_json::Value>) -> std::collections::HashMap<String, String>
{
    get_data_array(data).iter()
        .map(|game| (
            string_or_error(game.get("id")),
            string_or_error(game.get("name"))
        )).collect::<std::collections::HashMap<String, String>>()
}

fn validate_user(data: &Result<serde_json::Value, serde_json::Value>) -> String
{
    get_data_array(data).get(0)
        .and_then(|user| user.get("id"))
        .and_then(|id| id.as_str())
        .map_or("".to_string(), |s| s.to_string())
}

#[allow(dead_code)]
pub fn get_channel_info(id: &str) -> std::vec::Vec<Channel>
{
    let follow_url = "https://api.twitch.tv/helix/users/follows?first=100&from_id=".to_string();
    let json_resp = twitch_api_call(&format!("{}{}", follow_url, id));
    let v_channels = validate_follows(&json_resp);
    let mut streams_url = "https://api.twitch.tv/helix/streams?user_id=".to_string();
    let user_param = "&user_id=";
    for c in &v_channels {
        streams_url.push_str(&c);
        streams_url.push_str(user_param);
    }
    let ids: &str;
    if v_channels.len() > 0 {
        ids = &streams_url[0..streams_url.len() - user_param.len()];
    }
    else {
        ids = &streams_url;
    }
    let json_streams = twitch_api_call(ids);
    let mut game_url = "https://api.twitch.tv/helix/games?id=".to_string();
    let game_param = "&id=";
    let channel_info = validate_streams(&json_streams);
    for channel in &channel_info {
        game_url.push_str(&channel.game_id);
        game_url.push_str(game_param);
    }
    let json_games = twitch_api_call(&game_url[0..game_url.len() - game_param.len()]);
    let game_map = validate_games(&json_games);
    // Change game ids into game names before returning
    channel_info.into_iter().map(|mut chan| {
        chan.game = match game_map.get(&chan.game) {
            Some(s) => s.clone(),
            _ => "Unknown Game".to_string()
        };
        chan
    }).collect::<Vec<Channel>>()
}

pub fn get_channel_id(username: &str) -> String
{
    let userid_url = "https://api.twitch.tv/helix/users?login=";
    let json = twitch_api_call(&format!("{}{}", userid_url, username));
    validate_user(&json)
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn follows_test() {
        // Valid JSON to get 10 followers
        let sample: Result<serde_json::Value, serde_json::Value> = Ok(serde_json::json!({
            "total":63,
            "data":[
                {"from_id":"22104027","from_name":"jbawn","to_id":"124420521","to_name":"lcs","followed_at":"2019-09-08T23:32:41Z"},
                {"from_id":"22104027","from_name":"jbawn","to_id":"124425627","to_name":"lpl","followed_at":"2019-09-08T11:32:27Z"},
                {"from_id":"22104027","from_name":"jbawn","to_id":"423154266","to_name":"btscsgo","followed_at":"2019-05-25T18:52:06Z"},
                {"from_id":"22104027","from_name":"jbawn","to_id":"123484627","to_name":"iddqd","followed_at":"2019-05-09T22:14:09Z"},
                {"from_id":"22104027","from_name":"jbawn","to_id":"137512364","to_name":"OverwatchLeague","followed_at":"2019-05-04T02:02:57Z"},
                {"from_id":"22104027","from_name":"jbawn","to_id":"60900813","to_name":"Trikslyr","followed_at":"2019-04-23T02:46:18Z"},
                {"from_id":"22104027","from_name":"jbawn","to_id":"2400710","to_name":"MembTV","followed_at":"2019-03-11T23:21:55Z"},
                {"from_id":"22104027","from_name":"jbawn","to_id":"91402599","to_name":"Hera_Aoc","followed_at":"2019-03-08T03:10:57Z"},
                {"from_id":"22104027","from_name":"jbawn","to_id":"30382440","to_name":"TheViper","followed_at":"2019-03-06T03:13:19Z"},
                {"from_id":"22104027","from_name":"jbawn","to_id":"31680543","to_name":"mblaoc","followed_at":"2019-02-25T19:17:53Z"}],
            "pagination":{"cursor":"eyJiIjpudWxsLCJhIjp7IkN1cnNvciI6IjE1NTExMjIyNzM1NTY3Mzk1NDcifX0"}}));
        let ret = validate_follows(&sample);
        assert!(ret.len() == 10);
        assert!(ret[0] == "124420521");
    }

    #[test]
    fn streams_test() {
        let sample: Result<serde_json::Value, serde_json::Value> = Ok(serde_json::json!({
            "data":[
                {"game_id":"21779","id":"36357419424","language":"en","started_at":"2019-12-05T23:11:49Z","tag_ids":["6ea6bca4-4712-4ab9-a906-e3336a9d8039"],"thumbnail_url":"https://static-cdn.jtvnw.net/previews-ttv/live_user_riotgames-{width}x{height}.jpg","title":"2019 All-Star Event: Day 1","type":"live","user_id":"36029255","user_name":"Riot Games","viewer_count":13831},
                {"game_id":"32399","id":"36355873072","language":"en","started_at":"2019-12-05T17:21:59Z","tag_ids":["6ea6bca4-4712-4ab9-a906-e3336a9d8039"],"thumbnail_url":"https://static-cdn.jtvnw.net/previews-ttv/live_user_imaqtpie-{width}x{height}.jpg","title":"i","type":"live","user_id":"24991333","user_name":"imaqtpie","viewer_count":5356},
                {"game_id":"488552","id":"36356929104","language":"en","started_at":"2019-12-05T21:50:26Z","tag_ids":["1eba3cfe-51cc-460a-8259-bc8bb987f904","6ea6bca4-4712-4ab9-a906-e3336a9d8039","69f89b9a-6efd-4520-a8ad-1047ea2b79e0"],"thumbnail_url":"https://static-cdn.jtvnw.net/previews-ttv/live_user_iddqd-{width}x{height}.jpg","title":"oh hai tharr, it me it ur boy iddqd !dreams","type":"live","user_id":"123484627","user_name":"iddqd","viewer_count":1455},
                {"game_id":"11557","id":"36356856720","language":"en","started_at":"2019-12-05T21:35:35Z","tag_ids":["7cefbf30-4c3e-4aa7-99cd-70aabb662f27","6ea6bca4-4712-4ab9-a906-e3336a9d8039"],"thumbnail_url":"https://static-cdn.jtvnw.net/previews-ttv/live_user_zfg1-{width}x{height}.jpg","title":"Zelda's Letter SRM","type":"live","user_id":"8683614","user_name":"Zfg1","viewer_count":1429},
                {"game_id":"29595","id":"36356485696","language":"en","started_at":"2019-12-05T20:12:37Z","tag_ids":["6ea6bca4-4712-4ab9-a906-e3336a9d8039"],"thumbnail_url":"https://static-cdn.jtvnw.net/previews-ttv/live_user_beyondthesummit-{width}x{height}.jpg","title":"RERUN: Cignal Ultra vs TNC Predator Game 1 - DreamLeague S13 SEA Qualifiers: 3rd Place Match","type":"live","user_id":"29578325","user_name":"BeyondTheSummit","viewer_count":536},
                {"game_id":"512988","id":"265091969","language":"en","started_at":"2019-12-05T17:00:31Z","tag_ids":["6ea6bca4-4712-4ab9-a906-e3336a9d8039"],"thumbnail_url":"https://static-cdn.jtvnw.net/previews-ttv/live_user_membtv-{width}x{height}.jpg","title":"1v1 DE RANKED PRO - !loots !prime !goal","type":"live","user_id":"2400710","user_name":"MembTV","viewer_count":358},
                {"game_id":"32959","id":"36355737872","language":"en","started_at":"2019-12-05T16:37:42Z","tag_ids":["6ea6bca4-4712-4ab9-a906-e3336a9d8039"],"thumbnail_url":"https://static-cdn.jtvnw.net/previews-ttv/live_user_trikslyr-{width}x{height}.jpg","title":"!SELLOUT DAY","type":"live","user_id":"60900813","user_name":"Trikslyr","viewer_count":333},
                {"game_id":"504461","id":"36355170960","language":"en","started_at":"2019-12-05T13:07:05Z","tag_ids":["6ea6bca4-4712-4ab9-a906-e3336a9d8039"],"thumbnail_url":"https://static-cdn.jtvnw.net/previews-ttv/live_user_btssmash-{width}x{height}.jpg","title":"RERUN: varun/Von vs Nairo/Samsora - Losers' Round 3: Ultimate Doubles Top 16 - Mainstage","type":"live","user_id":"214062798","user_name":"btssmash","viewer_count":27},
                {"game_id":"27473","id":"36356763920","language":"en","started_at":"2019-12-05T21:16:01Z","tag_ids":["67259b26-ff83-444e-9d3c-faab390df16f","0cb7e377-a308-44e8-91f7-07eea1d96129","6ea6bca4-4712-4ab9-a906-e3336a9d8039"],"thumbnail_url":"https://static-cdn.jtvnw.net/previews-ttv/live_user_talkingrecklesspodcast-{width}x{height}.jpg","title":"Still Trapped in the ANIME ZONE | !D&D podcast is live now!","type":"live","user_id":"23971565","user_name":"TalkingRecklessPodcast","viewer_count":13}],
            "pagination":{"cursor":"eyJiIjpudWxsLCJhIjp7Ik9mZnNldCI6OX19"}}));
        let ret = validate_streams(&sample);
        assert!(ret.len() == 9);
        assert!(ret[0].user_name == "Riot Games");
    }

    #[test]
    fn games_test() {
        let sample: Result<serde_json::Value, serde_json::Value> = Ok(serde_json::json!({
            "data":[
                {"box_art_url":"https://static-cdn.jtvnw.net/ttv-boxart/Dota%202-{width}x{height}.jpg","id":"29595","name":"Dota 2"},
                {"box_art_url":"https://static-cdn.jtvnw.net/ttv-boxart/League%20of%20Legends-{width}x{height}.jpg","id":"21779","name":"League of Legends"},
                {"box_art_url":"https://static-cdn.jtvnw.net/ttv-boxart/Street%20Fighter%20V-{width}x{height}.jpg","id":"488615","name":"Street Fighter V"},
                {"box_art_url":"https://static-cdn.jtvnw.net/ttv-boxart/World%20of%20Warcraft-{width}x{height}.jpg","id":"18122","name":"World of Warcraft"},
                {"box_art_url":"https://static-cdn.jtvnw.net/ttv-boxart/Overwatch-{width}x{height}.jpg","id":"488552","name":"Overwatch"},
                {"box_art_url":"https://static-cdn.jtvnw.net/ttv-boxart/Super%20Smash%20Bros.%20Ultimate-{width}x{height}.jpg","id":"504461","name":"Super Smash Bros. Ultimate"},
                {"box_art_url":"https://static-cdn.jtvnw.net/ttv-boxart/./Age%20of%20Empires%20II:%20Definitive%20Edition-{width}x{height}.jpg","id":"512988","name":"Age of Empires II: Definitive Edition"},
                {"box_art_url":"https://static-cdn.jtvnw.net/ttv-boxart/./The%20Legend%20of%20Zelda:%20Ocarina%20of%20Time-{width}x{height}.jpg","id":"11557","name":"The Legend of Zelda: Ocarina of Time"}]}));
        let ret = validate_games(&sample);
        assert!(ret.len() == 8);
        assert!(ret["29595"] == "Dota 2");
    }
}

